/*InPut
    - Số tiền 1 ngày: 100.000
    - Số ngày làm: 2

Steps
    - Số tiền 1 ngày * số ngày làm

Output
    -200.000

*/

function tinhTien() {
  var workingDay = document.getElementById("day").value;

  var salary = workingDay * 100000;

  document.getElementById("salary").innerHTML = "Tổng Tiền Lương Là: " + salary;
}
