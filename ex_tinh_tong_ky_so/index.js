/*InPut
   Nhập số 24

Steps
    lấy hàng đơn vị: 4
    Lấy hàng chục: 2
    Tính tổng: 4+2

Output
    6

*/

function tinh() {
  var number = document.getElementById("number").value * 1;

  var donVi = number % 10;

  var chuc = parseInt(number / 10);

  var tong = donVi + chuc;

  document.getElementById("result").innerHTML = "Tổng 2 ký số là: " + tong;
}
