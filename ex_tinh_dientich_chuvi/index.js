/*InPut
    Chieu dai: 5
    Chieu rộng: 7

Steps
    -Chu vi: (5+7) * 2
    - Dien Tích: 5 * 7

Output
    Chu Vi: 24
    Dien Tích: 35

*/

function tinh() {
  var chieuDai = document.getElementById("chieu-dai").value * 1;

  var chieuRong = document.getElementById("chieu-rong").value * 1;

  var chuVi = (chieuDai + chieuRong) * 2;

  var dienTich = chieuDai * chieuRong;

  document.getElementById("result_chu_vi").innerHTML =
    "Chu Vi Hình Chữ Nhật Là: " + chuVi;
  document.getElementById("result_dien_tich").innerHTML =
    "Diện Tích Hình Chữ Nhật Là: " + dienTich;
}
