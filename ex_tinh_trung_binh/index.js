/*InPut
    - Nhập 5 số: 1, 2, 3, 4 ,5

Steps
    - Cộng tổng 5 số
    -Lấy tổng 5 số chia 5

Output
    -kết quả trung bình: 3

*/

function tinhTrungBinh() {
  var number1 = document.getElementById("number-1").value * 1;
  var number2 = document.getElementById("number-2").value * 1;
  var number3 = document.getElementById("number-3").value * 1;
  var number4 = document.getElementById("number-4").value * 1;
  var number5 = document.getElementById("number-5").value * 1;

  var result = number1 + number2 + number3 + number4 + number5;

  document.getElementById("result").innerHTML =
    "Giá Trị Trung Bình Là: " + result / 5;
}
